<?php

namespace app\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

class AdminLTE extends AssetBundle {
    
    public $sourcePath = '@bower/admin-lte';

    public $js = [
//        '//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
//        'js/plugins/morris/morris.min.js',
//        'js/plugins/sparkline/jquery.sparkline.min.js',
//        'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
//        'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
//        'js/plugins/jqueryKnob/jquery.knob.js',
//        'js/plugins/daterangepicker/daterangepicker.js',
//        'js/plugins/datepicker/bootstrap-datepicker.js',
//        'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
//        'js/plugins/iCheck/icheck.min.js',
        'js/AdminLTE/app.js',
    ];
    
    public $css = [
//        'css/font-awesome.min.css',
//        'css/ionicons.min.css',
//        'css/morris/morris.css',
//        'css/jvectormap/jquery-jvectormap-1.2.2.css',
//        'css/datepicker/datepicker3.css',
//        'css/daterangepicker/daterangepicker-bs3.css',
//        'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'css/AdminLTE.css'
    ];
    
    public function __construct($config = array()) {
        parent::__construct($config);
        
        $this->depends[] = BootstrapAsset::className();
    }
}